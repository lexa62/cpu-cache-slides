### Why software developers should care about CPU caches

---

@snap[north]
@size[1.5em](Matrix traversal)
@snapend

@snap[west span-50 text-center]
@size[0.8em](Row-wise)
![IMAGE](assets/img/row_wise.png)
@snapend

@snap[midpoint  text-center]
@size[1.2em](VS)
@snapend

@snap[east span-50 text-center]
@size[0.8em](Column-wise)
![IMAGE](assets/img/column_wise.png)
@snapend

---
@snap[north-east span-100]
Matrix traversal
@snapend

```go zoom-8
const MAX = 10000

func RowWiseTraversal() {
	var arr [MAX][MAX]int

	for i := 0; i < MAX; i++ {
		for j := 0; j < MAX; j++ {
			arr[i][j]++
		}
	}
}

func ColumnWiseTraversal() {
	var arr [MAX][MAX]int

	for i := 0; i < MAX; i++ {
		for j := 0; j < MAX; j++ {
			arr[j][i]++
		}
	}
}
```

@snap[south span-100]
@[3-11](Just increment each cell)
@[13-21](Just increment each cell)
@snapend

---

@snap[span-100]
```bash
RowWiseTraversal-8      10	 108692912 ns/op # 8.5x faster 🤔
ColumnWiseTraversal-8    2	 928959141 ns/op
```
@snapend

---

@snap[north-east span-100]
Scalability fail
@snapend

```go
const limit = 10000000000

func SerialSum() int {
	sum := 0
	for i := 0; i < limit; i++ {
		sum += i
	}
	return sum
}
```

---

@snap[north-east span-100]
Scalability fail
@snapend

```go
func ConcurrentSum() int {
	n := runtime.GOMAXPROCS(0)
	sums := make([]int, n)
	wg := sync.WaitGroup{}

	for i := 0; i < n; i++ {
		wg.Add(1)
		go func(i int) {
			start := (limit / n) * i
			end := start + (limit / n)
			for j := start; j < end; j++ {
				sums[i] += j
			}
			wg.Done()
		}(i)
	}
	wg.Wait()

	sum := 0
	for _, s := range sums {
		sum += s
	}
	return sum
}
```

@[2-4]
@[5-17]
@[19-23]

---

@snap[span-100]
```bash
SerialSum-8              1	5517142551 ns/op # 1.8x faster 🤔
ConcurrentSum-8          1	9890355087 ns/op
```
@snapend

---
@snap[north span-100]
### CPU Caches
@snapend

@snap[west span-100]
#### Small amounts of unusually fast memory
@ul
+ Generally hold contents of recently accessed memory locations
+ Access latency much smaller than for main memory
@ulend
@snapend

---
@snap[north span-100]
### CPU Caches
@snapend

@snap[west span-100]
#### Three common types:
@ul
+ Data (D-cache, D$)
+ Instruction (I-cache, I$)
+ Translation lookaside buffer (TLB)
  + Caches virtual -> real address translations
@ulend
@snapend

---
@snap[north span-100]
### Cache Hierarchies
@snapend

@snap[west span-100]
#### E.g., Intel Core i7-7820HQ processor:
@ul
+ 128 KB <span style="color:green">L1 I-cache</span>, 128 KB <span style="color:green">L1 D-cache</span> per core
  + Shared by 2 HW threads
+ 1 MB <span style="color:green">L2 cache</span> per core
  + Holds both instructions and data
  + Shared by 2 HW threads
+ 8 MB <span style="color:green">L3 cache</span>
  + Holds both instructions and data
  + Shared by 4 cores (8 HW threads)
@ulend
@snapend

---

@snap[north span-100]
### Intel Core cache hierarchy
@snapend

@snap[west span-100 text-center]
![IMAGE](assets/img/cache_2.png)
@snapend

---

@snap[north span-100]
### CPU Caches latency
@snapend

@snap[west span-100]
#### Caches much faster than main memory:
@ul
+ L1 latency is 4 cycles
+ L2 latency is 12 cycles
+ L3 latency is 42 cycles
+ Main memory latency is 107 cycles
  + <span style="color:green">27 times slower than L1</span>
@ulend
@snapend

---
@snap[north span-100]
### Cache Lines
@snapend

@snap[west span-100]
@ul
+ Caches consist of lines, each holding multiple adjacent words
  + On Core i7, cache lines hold 64 bytes
+ Main memory read/written in terms of cache lines
  + Read byte not in cache => read full cache line from main memory
  + Write byte => write full cache line to main memory
@ulend
@snapend

<!-- matrix traversal, line frefetching -->

---

@snap[north span-100]
### Cache Coherency
@snapend

@snap[west span-100 text-09]
@ul
+ Assume both cores have cached the value at address *A*
  + Whether in L1 or L2 makes no difference
+ Consider:
  + Core 0 writes to *A*
  + Core 1 reads *A*
+ *What value does Core 1 read?*
+ Hardware invalidates Core 1’s cached value when Core 0 writes to A
  + It then puts the new value in Core 1’s cache(s)
  + <span style="color:green">But it takes time</span>
@ulend
@snapend

---

@snap[north span-100]
### False Sharing
@snapend

@snap[west span-100 text-09]
@ul
+ Suppose Core 0 accesses *A* and Core 1 accesses *A+1*
  + Independent pieces of memory, concurrent access is safe
+ But *A* and *A+1* probably map to the same cache line:
  + If so, Core 0’s writes to *A* invalidates *A+1*’s cache line in Core 1
  + And vice versa
  + This is false sharing
@ulend
@snapend

---

@snap[north span-100]
### False Sharing
@snapend

@snap[west span-100]
@ul
+ Problems arise only when *all* are true:
  + Independent values/variables fall on one cache line
  + Different cores concurrently access that line
  + Frequently
  + At least one is a writer
@ulend
@snapend

---

```go
func ConcurrentSumRightWay() int {
	n := runtime.GOMAXPROCS(0)
	ch := make(chan int, n)
	wg := sync.WaitGroup{}

	for i = 0; i < n; i++ {
		wg.Add(1)

		go func(i int) {
			counter := 0
			start := (limit / n) * i
			end := start + (limit / n)
			for j := start; j < end; j++ {
				counter += j
			}
			ch <- counter
			wg.Done()
		}(i)
	}
	wg.Wait()
	close(ch)

	sum := 0
	for s := range ch {
		sum += s
	}
	return sum
}
```
@[2-4]
@[6-21]
@[23-27]

---

@snap[span-100]
```bash
SerialSum-8              1	5517142551 ns/op # 1.8x faster
ConcurrentSum-8          1	9890355087 ns/op
ConcurrentSumRightWay-8  2	 741068182 ns/op # 13.3x faster 🔥
```
@snapend

---

@snap[north span-100]
### Summary
@snapend

@snap[west span-100]
@ul
+ Where practical, employ linear array traversals
+ Avoid cache line sharing between threads
+ Use as much of a cache line as possible
@ulend
@snapend

---

@snap[north span-100]
### Further Information
@snapend

@snap[west span-100]
@ul
+ What Every Programmer Should Know About Memory, Ulrich Drepper, 21 November 2007
+ Structured Computer Organization, Andrew S. Tanenbaum
@ulend
@snapend

---


@snap[midpoint span-100]
# Questions?
@snapend
